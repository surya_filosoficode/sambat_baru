package com.example.sambat.User_directory;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.sambat.R;
import com.example.sambat.internal_library.Config.SessionCheck;

public class UserEdit extends AppCompatActivity {
    EditText et_nama, et_email, et_alamat, et_no_telphone, et_profesi;
    Button btn_edit;

    String TAG = "UserEdit", id_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_edit_main);

        et_nama         = (EditText) findViewById(R.id.et_nama);
        et_email        = (EditText) findViewById(R.id.et_email);
        et_alamat       = (EditText) findViewById(R.id.et_alamat);
        et_no_telphone  = (EditText) findViewById(R.id.et_no_telpon);
        et_profesi      = (EditText) findViewById(R.id.et_profesi);

        btn_edit = (Button) findViewById(R.id.btn_edit);

        get_data();

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    public void get_data() {
        String[] data_session = new SessionCheck().seesionLoginChecker(UserEdit.this);
        Log.e(TAG, "onClick: " + data_session[1]);
        if (data_session != null) {

            String id_user_     = data_session[1];
            String nama_        = data_session[2];
            String email_       = data_session[3];
            String alamat_      = data_session[4];
            String profesi_     = data_session[5];
            String no_telpon_   = data_session[6];
            String username_    = data_session[7];

            id_user = id_user_;

            et_nama.setText(nama_);
            et_email.setText(email_);
            et_alamat.setText(alamat_);
            et_profesi.setText(profesi_);
            et_no_telphone.setText(no_telpon_);
//            username.setText(username_);
        }
    }

}
