package com.example.sambat;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sambat.global_variable.MessageVariabe;
import com.example.sambat.internal_library.Check_library.CheckConn;
import com.example.sambat.internal_library.Config.Retrofit_client;
import com.example.sambat.internal_library.Retrofit_model.Login_request;
import com.example.sambat.global_variable.URLCollection;
import com.example.sambat.User_directory.UserMain;
import com.example.sambat.internal_library.refresh_activity.RefreshDataUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private TextView mTextMessage, btn_close;
    private Button btn_add;
    private Button btn_kirim;
    private TextView btn_login;
    private TextView btn_register;
    private TextView btn_vert;
    private Spinner sp_kategori;

    List<String> sp_kategori_array = new ArrayList<>();

    Dialog my_dialog;
    EditText et_username, et_pass;

    String admin_auth = "admin", password_auth = "1234";
    String username, password;

    String url_sambat = URLCollection.SAMBAT_MAIN_URL;
    String TAG = "LoginActivity";

    Login_request login_request;

    ProgressDialog progressDialog;
    MessageVariabe messageVariabe = new MessageVariabe();
    Intent intent;

//-------------------------------------------on_create--------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_main);

        my_dialog = new Dialog(this);

//        btn_add     = (Button) findViewById(R.id.show_popup);
        et_username = (EditText) findViewById(R.id.et_username);
        et_pass     = (EditText) findViewById(R.id.et_password);
        btn_login   = (TextView)  findViewById(R.id.btn_login);

        btn_register   = (TextView)  findViewById(R.id.btn_register);
        btn_vert   = (TextView)  findViewById(R.id.btn_vert);

//        btn_add.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                my_dialog.setContentView(R.layout.modal_add);
//                btn_close   = (TextView) my_dialog.findViewById(R.id.txtclose);
//                btn_kirim   = (Button) my_dialog.findViewById(R.id.btn_kirim);
//                sp_kategori = (Spinner) my_dialog.findViewById(R.id.sp_kategori);
//
//                sp_kategori_array.add("Kebocoran");
//                sp_kategori_array.add("Kurang Angin");
//                sp_kategori_array.add("Pecah Bannya");
//
//                // (3) create an adapter from the list
//                ArrayAdapter<String> adapter = new ArrayAdapter<String>(
//                        LoginActivity.this,
//                        android.R.layout.simple_dropdown_item_1line,
//                        sp_kategori_array
//                );
//                sp_kategori.setAdapter(adapter);
//
//                btn_close.setText("X");
//
//                btn_close.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        my_dialog.dismiss();
//                    }
//                });
//
//                btn_kirim.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Log.e("LoginActivity", "add");
//                    }
//                });
//
//                my_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                my_dialog.show();
//            }
//        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog = new ProgressDialog(LoginActivity.this) {
                    @Override
                    public void onBackPressed() {
                        finish();
                    }
                };

                progressDialog.setMessage(messageVariabe.MESSAGE_LOADING);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(false);
                progressDialog.show();

                username = et_username.getText().toString();
                password = et_pass.getText().toString();

//                Log.e(TAG, "btn_login : data_response : "+username);
//                Log.e(TAG, "btn_login : data_response : "+password);

                String set_auth = set_auth(admin_auth, password_auth);

//                Log.e(TAG, "btn_login : set_auth : "+set_auth);

                if(new CheckConn().isConnected(LoginActivity.this)){
                    if(admin_auth == "" || password_auth == "" || username == ""|| password == ""){
                        Log.e("btn_login", "input salah, ada yang kosong");
                    }else {

                        RequestBody username_request = RequestBody.create(MediaType.parse("multipart/form-data"), username);
                        RequestBody password_request = RequestBody.create(MediaType.parse("multipart/form-data"), password);

                        login_request = Retrofit_client.getClient_https(url_sambat).create(Login_request.class);
                        Call<ResponseBody> getdata = login_request.login_activity(set_auth, username_request, password_request);

                        getdata.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                progressDialog.dismiss();
                                try{
                                    JSONObject data_response;
                                    try {
                                        data_response = new JSONObject(response.body().string());
                                        Log.e(TAG, "btn_login : data_response : "+ data_response);

                                        boolean returnData = new RefreshDataUser().create_session(data_response, LoginActivity.this);
                                        Log.e(TAG, "onResponse: " + returnData);

                                        if(returnData){

                                            intent = new Intent(LoginActivity.this, MainActivity.class );
                                            startActivity(intent);
                                            finish();
                                            Toast.makeText(LoginActivity.this, messageVariabe.MESSAGE_SUCCESS_LOGIN,
                                                    Toast.LENGTH_LONG).show();
                                        }

                                    } catch (JSONException e) {
                                        Log.e(TAG, "btn_login : JSONException : "+ e.toString());
                                    } catch (IOException e) {
                                        Log.e(TAG, "btn_login : IOException :"+ e.toString());
                                    }
                                }catch (Exception e){
                                    Log.e(TAG, "btn_login : IOException :"+ e.toString());
                                    Toast.makeText(LoginActivity.this, messageVariabe.MESSAGE_ERROR_LOGIN,
                                            Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Log.e(TAG, "onFailure: " + t);
                            }
                        });
                    }
                }else {
                    Log.e("btn_login", "tidak ada koneksi");
                }

            }
        });

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(LoginActivity.this, RegisterMain.class);
                startActivity(intent);
//                finish();
            }
        });


        btn_vert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(LoginActivity.this, VerifikasiActivity.class);
                startActivity(intent);
//                finish();
            }
        });
    }
//-------------------------------------------on_create--------------------------------------


    public static String set_auth(String admin_auth, String password_auth) {
        String base = admin_auth +":"+ password_auth;
        String auth_header = "Basic "+ Base64.encodeToString(base.getBytes(), Base64.NO_WRAP);

       return auth_header;
    }

}
