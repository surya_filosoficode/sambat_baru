package com.example.sambat.internal_library.Retrofit_model;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;

/**
 * Created by USER on 22/03/2018.
 */

public interface Register_request {

    //---------------------------------------Register_Activity----------------------------------------

    @Multipart
    @POST("api/bagian/register")
    Call<ResponseBody> register_activity(@Header("Authorization") String authkey,
                                          @Part("username") RequestBody username,
                                          @Part("nama") RequestBody nama,
                                          @Part("email") RequestBody email,
                                          @Part("alamat") RequestBody alamat,
                                          @Part("no_telpon") RequestBody no_telpon,
                                          @Part("profesi") RequestBody profesi,
                                          @Part("password") RequestBody password);

    @Multipart
    @PUT("/api/bagian/password")
    Call<ResponseBody> ch_pass_activity(@Header("Authorization") String authkey,
                                         @Part("id_user") RequestBody id_user,
                                         @Part("password") RequestBody password);

}
