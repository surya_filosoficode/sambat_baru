package com.example.sambat.internal_library.refresh_activity;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.sambat.LoginActivity;
import com.example.sambat.global_variable.MessageVariabe;
import com.example.sambat.internal_library.Config.Session;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by USER on 14/05/2018.
 */

public class RefreshDataUser {

    String TAG = "RefreshDataUser";

    public Boolean create_session(JSONObject dataJson, Context context) {
        try {
            if (dataJson != null) {
//                Log.e(TAG, "create_session: " + dataJson.toString());

                String logged_in = dataJson.getString("logged_in");
//                Log.e(TAG, "create_session : "+logged_in);

                String id_user = null;

                String nama = null;
                String email = null;
                String alamat = null;
                String profesi = null;
                String no_telpon = null;
                String username = null;
//
                if (logged_in != "yesGetMeLoginBaby") {
                    //main data
                    id_user = dataJson.getString("id_user");
                    nama    = dataJson.getString("nama");
                    email   = dataJson.getString("email");
                    alamat  = dataJson.getString("alamat");
                    profesi   = dataJson.getString("profesi");
                    no_telpon = dataJson.getString("no_telpon");
                    username  = dataJson.getString("username");

                    logged_in  = dataJson.getString("logged_in");

                    Log.e(TAG, "create_session"+logged_in);

                    new Session().loginSession(context,
                            id_user, nama, email, alamat, profesi, no_telpon, username, logged_in);

                    return true;
                }

            }
        } catch (JSONException e) {
            Log.e(TAG, "create_session: " + e.getMessage());
        }

        return false;
    }

}
