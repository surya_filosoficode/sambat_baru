package com.example.sambat.internal_library.Config;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

//import static android.content.Context.MODE_PRIVATE;

/**
 * Created by USER on 23/03/2018.
 */

public class Session {

    String TAG = "Session";

    public void loginSession(Context context, String id_user, String nama, String email,
                             String alamat, String profesi, String no_telpon, String username, String logged_in) {

        Log.e(TAG, "loginSession: run");
        try {
            Log.e(TAG, "loginSession: run");
            SharedPreferences sharedPref = context.getSharedPreferences("Login", Context.MODE_PRIVATE);
            Log.e(TAG, "loginSession: sharedPref: " + sharedPref);
            SharedPreferences.Editor editor = sharedPref.edit();
            Log.e(TAG, "loginSession: editor: " + editor);

            editor.putString("id_user"  , id_user);
            editor.putString("nama"     , nama);
            editor.putString("email"    , email);
            editor.putString("alamat"   , alamat);
            editor.putString("profesi"  , profesi);
            editor.putString("no_telpon", no_telpon);
            editor.putString("username" , username);

            editor.putString("logged_in", logged_in);
            editor.apply();
        } catch (Exception e) {
            Log.e(TAG, "loginSession: " + e.getMessage());
        }
    }

    public void sessionDestroy(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences("Login", Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();
        Log.e(TAG, "sessionDestroy: session destroy");
    }
}
