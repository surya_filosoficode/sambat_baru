package com.example.sambat.internal_library.Retrofit_model;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by USER on 22/03/2018.
 */

public interface Login_request {

    //---------------------------------------login activity----------------------------------------

    @Multipart
    @POST("api/bagian/login")
    Call<ResponseBody> login_activity(@Header("Authorization") String authkey,
                                      @Part("username") RequestBody username,
                                      @Part("password") RequestBody password);

}
