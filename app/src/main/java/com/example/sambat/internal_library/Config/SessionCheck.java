package com.example.sambat.internal_library.Config;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by USER on 23/03/2018.
 */

public class SessionCheck {
    String TAG = "SessionCheck";

    public String[] seesionLoginChecker(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("Login", Context.MODE_PRIVATE);
        Log.e(TAG, "seesionLoginChecker: " + prefs);
        if (prefs != null) {
            String logged_in = prefs.getString("logged_in", null);
            if (logged_in != null) {

                String id_user  = prefs.getString("id_user", null);//"No name defined" is the default value.
                String nama     = prefs.getString("nama", null); //0 is the default value.
                String email    = prefs.getString("email", null); //0 is the default value.
                String alamat   = prefs.getString("alamat", null);//"No name defined" is the default value.
                String profesi  = prefs.getString("profesi", null); //0 is the default value.
                String no_telpon = prefs.getString("no_telpon", null);
                String username = prefs.getString("username", null);//"No name defined" is the default value.

                Log.e(TAG, "seesionLoginChecker: " + id_user);
                String[] session_val = {logged_in,
                        id_user, nama, email, alamat, profesi, no_telpon, username};
                return session_val;
            }
        }
        return null;
    }


}
