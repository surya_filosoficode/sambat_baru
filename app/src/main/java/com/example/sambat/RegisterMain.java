package com.example.sambat;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sambat.User_directory.UserMain;
import com.example.sambat.global_variable.MessageVariabe;
import com.example.sambat.global_variable.URLCollection;
import com.example.sambat.internal_library.Check_library.CheckConn;
import com.example.sambat.internal_library.Config.Retrofit_client;
import com.example.sambat.internal_library.Retrofit_model.Login_request;
import com.example.sambat.internal_library.Retrofit_model.Register_request;
import com.example.sambat.internal_library.refresh_activity.RefreshDataUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterMain extends AppCompatActivity {
    EditText et_username, et_nama, et_email, et_alamat, et_no_telphone, et_profesi, et_password;
    TextView btn_register;

    String id_user, logged_in;

    String TAG = "RegisterMain";
    String url_sambat = URLCollection.SAMBAT_MAIN_URL;

    String admin_auth = "admin", password_auth = "1234";

    ProgressDialog progressDialog;
    MessageVariabe messageVariabe = new MessageVariabe();
    Intent intent;

    Register_request register_request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_main);

//        et_username    = (EditText) findViewById(R.id.et_username);
        et_nama        = (EditText) findViewById(R.id.et_nama);
        et_email       = (EditText) findViewById(R.id.et_email);
        et_alamat      = (EditText) findViewById(R.id.et_alamat);
        et_no_telphone = (EditText) findViewById(R.id.et_no_telpon);
        et_profesi     = (EditText) findViewById(R.id.et_profesi);
        et_password    = (EditText) findViewById(R.id.et_password);

        btn_register = (TextView) findViewById(R.id.btn_register);

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog = new ProgressDialog(RegisterMain.this) {
                    @Override
                    public void onBackPressed() {
                        finish();
                    }
                };

                progressDialog.setMessage(messageVariabe.MESSAGE_LOADING);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(false);
                progressDialog.show();

                String username     = et_email.getText().toString();
                String nama         = et_nama.getText().toString();
                String email        = et_email.getText().toString();
                String alamat       = et_alamat.getText().toString();
                String no_telphone  = et_no_telphone.getText().toString();
                String profesi      = et_profesi.getText().toString();
                String password     = et_password.getText().toString();

                String set_auth = set_auth(admin_auth, password_auth);


                if(new CheckConn().isConnected(RegisterMain.this)){
                    if(admin_auth == "" || password_auth == "" || username == ""|| password == ""){
                        Log.e("btn_login", "input salah, ada yang kosong");
                    }else {

                        RequestBody username_request    = RequestBody.create(MediaType.parse("multipart/form-data"), username);
                        RequestBody nama_request        = RequestBody.create(MediaType.parse("multipart/form-data"), nama);
                        RequestBody email_request       = RequestBody.create(MediaType.parse("multipart/form-data"), email);
                        RequestBody alamat_request      = RequestBody.create(MediaType.parse("multipart/form-data"), alamat);
                        RequestBody no_telphone_request = RequestBody.create(MediaType.parse("multipart/form-data"), no_telphone);
                        RequestBody profesi_request     = RequestBody.create(MediaType.parse("multipart/form-data"), profesi);
                        RequestBody password_request    = RequestBody.create(MediaType.parse("multipart/form-data"), password);

                        register_request = Retrofit_client.getClient_https(url_sambat).create(Register_request.class);
                        Call<ResponseBody> getdata = register_request.register_activity(set_auth, username_request, nama_request,
                                                                                        email_request, alamat_request, no_telphone_request,
                                                                                        profesi_request, password_request);

                        getdata.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                progressDialog.dismiss();
                                try {

                                    JSONObject data_response;
                                    try {
                                        data_response = new JSONObject(response.body().string());
                                        Log.e(TAG, "btn_login : data_response : "+ data_response);

                                        Boolean status = data_response.getBoolean("status");

                                        if(status){
                                            intent = new Intent(RegisterMain.this, LoginActivity.class);
                                            startActivity(intent);
                                            finish();
                                            Toast.makeText(RegisterMain.this, messageVariabe.MESSAGE_SUCCESS_REGISTER,
                                                    Toast.LENGTH_LONG).show();
                                        }else {
                                            Toast.makeText(RegisterMain.this, messageVariabe.MESSAGE_ERROR_REGISTER,
                                                    Toast.LENGTH_LONG).show();
                                        }

                                    } catch (JSONException e) {
                                        Log.e(TAG, "btn_login : JSONException : "+ e.toString());
                                    } catch (IOException e) {
                                        Log.e(TAG, "btn_login : IOException :"+ e.toString());
                                    }

//                                    Log.e(TAG, "ResponseBody" + response.body().string());



                                }catch (Exception e){
                                    Toast.makeText(RegisterMain.this, messageVariabe.MESSAGE_ERROR_REGISTER,
                                            Toast.LENGTH_LONG).show();
                                }


//                                JSONObject data_response;
//                                try {
//                                    data_response = new JSONObject(response.body().string());
//                                    Log.e(TAG, "btn_login : data_response : "+ data_response);

//                                    progressDialog.dismiss();
//                                    intent = new Intent(RegisterMain.this, LoginActivity.class);
//                                    startActivity(intent);
//                                    finish();
//                                    Toast.makeText(RegisterMain.this, messageVariabe.MESSAGE_SUCCESS_LOGIN,
//                                                Toast.LENGTH_LONG).show();

//                                } catch (JSONException e) {
//                                    Log.e(TAG, "btn_login : JSONException : "+ e.toString());
//                                } catch (IOException e) {
//                                    Log.e(TAG, "btn_login : IOException :"+ e.toString());
//                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Log.e(TAG, "onFailure: " + t);
                            }
                        });
                    }
                }else {
                    Log.e("btn_login", "tidak ada koneksi");
                }
            }
        });
    }

    public static String set_auth(String admin_auth, String password_auth) {
        String base = admin_auth +":"+ password_auth;
        String auth_header = "Basic "+ Base64.encodeToString(base.getBytes(), Base64.NO_WRAP);

        return auth_header;
    }
}
